// Stringhe localizzate necessarie a determinare a quale paragrafo collegare l'immagine iniziale
exports.stringheImmagineIniziale = {
	it: ['immagine iniziale', 'immagini iniziali', 'illustrazione iniziale'],
	ru: ['в начале статьи']
}

// Stringhe localizzate necessarie a determinare a quale paragrafo collegare un riquadro
exports.stringheImmagineRiquadro = {
	it: ['riquadr'],
	ru: ['мотрите рамк']
}

// Mime-Type validi per upload immagini
exports.mimetypeValidi = [ 'image/jpeg', 'image/jpg', 'image/bmp', 'image/png' ]

// Tipi di contenuto estraibili da WOL
exports.tipiContenuto = [ 'illustrazioni', 'riferimenti', 'link', 'video' ]

// Valori predefiniti per la configurazione iniziale
exports.parametriRichiestaDefault = {
	cerca: [ 'illustrazioni' ],
	rapportoImmagini: 1.77778,
	sfondo: 'img',
	lunghezzaMaxRiferimenti: 400,
	lunghezzaMaxVideo: 15,
	origine: { offset: 0 },
	dividiImmaginiComposte: true
}

// Nome del file da restituire al client
exports.nomeFilePerRisposta = 'Contenuti.zip'

// Url base di WOL
exports.urlBaseWol = 'http://wol.jw.org/'

// Colore bianco (come valore intero in una Uint32Array)
exports.bianco = 4294967295
