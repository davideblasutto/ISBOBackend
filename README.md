# Illustrazioni, scritture e link dalla Biblioteca Online
# Backend

Estrae illustrazioni, scritture, video e link da un articolo di wol.jw.org.

Accessibile ai seguenti indirizzi:
 * https://isbo.info.tm
 * http://isbo.info.tm:8080

Per questioni di retrocompatibilità il backend è ancora accessibile anche a questi indirizzi:
 * http://isbo.mooo.com:8080

## Parametri
Il programma accetta come parametro un oggetto "parametrizzato" di questo tipo:


```
{
	cerca: *Array di stringhe*,				// Determina che tipi di contenuti estrarre; l'array può contenere uno o più dei seguenti valori:
												- "illustrazioni"
												- "riferimenti"
												- "link"
												- "video"
	rapportoImmagini: *Numero*,				// Rapporto delle immagini - es.: 1.333 per 4:3, 1.778 per 16:9...
	sfondo": *Stringa*,						// Sfondo dei riferimenti - "img" o "nero"
	risoluzioneVideo": *Numero*,			// Risoluzione preferita dei video - 240, 360, 480 o 720
	lunghezzaMaxRiferimenti": *Numero*,		// Lunghezza massima (in caratteri) dei riferimenti; quelli più lunghi verranno ignorati
	lunghezzaMaxVideo": *Numero*,			// Lunghezza massima (in minuti) dei video; quelli più lunghi verranno ignorati o scaricati come link
	lingua": *Stringa*,						// Codice della lingua, presente nell'URL della Biblioteca Online subito dopo "wol.jw.org/"
	origine": {
		tipo: *Stringa*						// "adunanza", "riferimento" o "url"
		valore: *Stringa*					// Varia a seconda del tipo:
												- Per adunanza: "vcm", "w" o "sbc"
												- Per riferimento: "Matteo 24:14"
												- Per URL: l'URL di un articolo della Biblioteca Online
	}
}
```


L'oggetto deve essere "parametrizzato", cioè convertito in parametri HTTP GET. L'URL della richiesta sarà una cosa di questo tipo:

https://isbo.info.tm/?cerca[]=illustrazioni&cerca[]=link&cerca[]=video&rapportoImmagini=1.33&sfondo=img&risoluzioneVideo=480&lunghezzaMaxRiferimenti=400&lingua=it&origine[tipo]=adunanza&origine[valore]=vcm

## Installazione e aggiornamento su server
Lanciare lo script `/setup/installa.sh` **come root** per installare tutti i requisiti di sistema e il repo in ~/isbobackend.
Una volta installato saranno disponibili i comandi
 * `aggiorna`
 * `arresta`
 * `riavvia`

## Note
* I nomi dei file nell'archivio sono UTF-8. Se una volta estratti hanno mojibake è colpa del programma usato per l'estrazione (probabilmente Windows)
