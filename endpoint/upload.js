//
// ## Upload
// ## Parametri:
// ##  - immagini 					Array di file
// ##  - rapportoImmagini			Float
// ##  - dividiImmaginiComposte		Bool
//
module.exports = async function(richiesta, risposta) {

	try {

		// Fa in modo che richiesta.files.immagini sia un'array anche se c'è un solo file
		if (!Array.isArray(richiesta.files.immagini))
			richiesta.files.immagini = [ richiesta.files.immagini ]

		// Metadati delle immagini
		richiesta.metadati = { elementi: [] }

		for (var fileImmagine of richiesta.files.immagini) {

			// Per ogni file caricato
			logger.info('File caricato: ' + fileImmagine.name)

			// Elabora file solo se è un'immagine
			if (costanti.mimetypeValidi.includes(fileImmagine.mimetype)) {

				logger.info('Mime type valido: ' + fileImmagine.mimetype)

				// Salvo subito l'immagine in locale
				var percorsoLocale = path.join(cartellaRoot, 'tmp/img-' + rnd.generate(10) + '-' + fileImmagine.name)
				await fileImmagine.mv(percorsoLocale)

				// Aggiunge il file all'array dei metadati come 'illustrazione'
				// "src" è un percorso locale
				richiesta.metadati.elementi.push({
					tipo: 'illustrazione',
					locale: true,
					paragrafo: '',
					ordine: 0,
					titolo: fileImmagine.name.rimuoviEstensione(),
					src: percorsoLocale
				})

			}

		}

		logger.info('Immagini caricate da elaborare: ' + richiesta.metadati.elementi.length)

		// Parametri
		richiesta.parametri = {}
		richiesta.parametri.rapportoImmagini = richiesta.body.rapportoImmagini || costanti.parametriRichiestaDefault.rapportoImmagini
		if (richiesta.body.dividiImmaginiComposte == null)
			richiesta.parametri.dividiImmaginiComposte = costanti.parametriRichiestaDefault.dividiImmaginiComposte
		else
			richiesta.parametri.dividiImmaginiComposte = richiesta.body.dividiImmaginiComposte.toString().includes('true')

		// Elaborazione le immagini
		var elaboratore = new Elaboratore(richiesta)
		richiesta.contenuti = await elaboratore.elabora()

		// Se non sono stati trovati contenuti risponde con un messaggio di errore
		if (richiesta.contenuti.length == 0)
			throw 'Impossibile elaborare le immagini caricate'

		risposta.consegna(await zippatore.creaArchivio(richiesta))

	} catch (errore) { risposta.errore(errore) }

}
