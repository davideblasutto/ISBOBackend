//
// ## Elabora i contenuti in base alla configurazione ricevuta
//
module.exports = async function(richiesta, risposta) {

	try {

		// Se con la richiesta non è arrivato alcun dato mostra solo un link al repo
		if (isEmptyObject(richiesta.query)) {
			throw 'Maggiori informazioni su ' + require('../package.json').homepage
		}

		// Estrae i parametri della richiesta (cosa è stato chiesto)
		richiesta.parametri = richiesta.query

		// Parametri obbligatori
		if (!richiesta.parametri.lingua) {
			throw 'Parametro "lingua" obbligatorio'
		}
		if (!richiesta.parametri.origine) {
			throw 'Parametro "origine" { tipo, valore } obbligatorio'
		}

		// Valori predefiniti dei parametri non obbligatori
		richiesta.parametri.cerca = richiesta.parametri.cerca || costanti.parametriRichiestaDefault.cerca
		richiesta.parametri.rapportoImmagini = richiesta.parametri.rapportoImmagini || costanti.parametriRichiestaDefault.rapportoImmagini
		richiesta.parametri.sfondo = richiesta.parametri.sfondo || costanti.parametriRichiestaDefault.sfondo
		richiesta.parametri.lunghezzaMaxRiferimenti = richiesta.parametri.lunghezzaMaxRiferimenti || costanti.parametriRichiestaDefault.lunghezzaMaxRiferimenti
		richiesta.parametri.lunghezzaMaxVideo = richiesta.parametri.lunghezzaMaxVideo || costanti.parametriRichiestaDefault.lunghezzaMaxVideo
		richiesta.parametri.origine.offset = richiesta.parametri.origine.offset || costanti.parametriRichiestaDefault.origine.offset
		if (richiesta.parametri.dividiImmaginiComposte == null)
			richiesta.parametri.dividiImmaginiComposte = costanti.parametriRichiestaDefault.dividiImmaginiComposte
		else
			richiesta.parametri.dividiImmaginiComposte = richiesta.parametri.dividiImmaginiComposte.toString().includes('true')

		// In HTTP è più semplice gestire "cerca" come array di stringhe, ma da qui in poi devessere un'oggetto con dei flag, quindi viene convertito
		richiesta.parametri.cerca = utils.convertiArrayInOggettoConFlag(richiesta.parametri.cerca, costanti.tipiContenuto)

		// Estrazione metadati dall'origine e li salva in richiesta
		var parser = new WolParser(richiesta)
		richiesta.metadati = await parser.ottieniMetadati()

		// Elaborazione metadati per generazione contenuti e li salva in richiesta
		var elaboratore = new Elaboratore(richiesta)
		richiesta.contenuti = await elaboratore.elabora()

		// Se non sono stati trovati contenuti risponde con un messaggio di errore
		if (richiesta.contenuti.length == 0)
			throw 'Nessun contenuto trovato'

		risposta.consegna(await zippatore.creaArchivio(richiesta), costanti.nomeFilePerRisposta)

	} catch (errore) { risposta.errore(errore) }

}
