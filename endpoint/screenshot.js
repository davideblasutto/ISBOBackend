//
// ## Restituisce come immagine lo screenshot un url
//
module.exports = async function(richiesta, risposta) {

	try {

		logger.info('Richiesto screenshot pagina ' + richiesta.query.url + '...')

		// Prima di tutto "risolve" l'URL per ottenere l'indirizzo "finale"
		// Altrimenti webshot non riesce a creare lo screenshot
		logger.info('Cerco URL finale...')
		const ripostaUrl = await requestPromise({
			uri: richiesta.query.url,
			resolveWithFullResponse: true
		})

		const url = ripostaUrl.request.uri.href
		logger.info('URL finale per screenshot: ' + url)

		const percorsoLocale = path.join(cartellaRoot, 'tmp/screenshot-' + rnd.generate(10) + '.png')

		const opzioniWebshot = {
			renderDelay: richiesta.query.delay,
			windowSize: {
				width: parseInt(richiesta.query.larghezza),
				height: parseInt(richiesta.query.larghezza / richiesta.query.rapporto)
			},
			shotSize: {
				width: 'window',
				height: 'window'
			},
			phantomConfig: {
				'ignore-ssl-errors': true,
				'ssl-protocol': 'any'
			}
		}

		logger.info('Dimensioni screenshot:')
		logger.info(opzioniWebshot.windowSize)

		if (richiesta.query.delay)
			logger.info('Delay: ' + richiesta.query.delay + ' ms')

		// Crea lo screenshot
		logger.info('Avvio generazione screenshot...')
		webshot(url, percorsoLocale, opzioniWebshot, function(errore) {

			if (errore)
				throw errore
			else
				risposta.consegna(percorsoLocale, 'screenshot.png')

		})

	} catch (errore) { risposta.errore(errore) }

}
