//
// ## Restituisce il contenuto di un url, con in più l'header CORS
//
module.exports = function(richiesta, risposta) {

	logger.info('Richiesta pagina ' + richiesta.query.url + ' con header CORS aggiuntivo...')
	// Ha più senso usare request che request-promise
	// Vedi https://www.npmjs.com/package/request-promise#api-in-detail
	richiesta.pipe(request(richiesta.query.url)).pipe(risposta)

}
