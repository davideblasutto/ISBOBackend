//
// ## Restituisce la versione del backend
// ## (Letta dal changelog)
//
module.exports = function(richiesta, risposta) {

	fs.readFile(path.join(cartellaRoot, 'CHANGELOG.md'), function(err, changelog){

		const versione = changelog.toString().replace(/[\r\n]/g, ' ').split(' ')[1]
		logger.info('Richiesta versione backend (' + versione + ')...')
		// Prende la seconda parola della prima riga del changelog
		risposta.end( '"' + versione + '"')

	})

}
