# 2.3.0 (16/06/2018)
 * Log su MySQL delle richieste e delle risposte

# 2.2.10 (11/06/2018)
 * Adattato parser a nuova struttura di WOL
 * Bugfix: l'elaborazione delle adunanze di tipo `sbc` non prendeva gli url corretti

# 2.2.9 (09/04/2018)
 * Migliorato script [`installa.sh`](setup/installa.sh)
 	* Alias persistenti (chiuso issue #11)
	* Riavvio quotidiano del server e avvio automatico di `forever` al boot (chisuo issue #10)

# 2.2.8 (03/03/2018)
 * Eventuali errori in sotto-richieste HTTP non bloccano più l'elaborazione - Semplicemente i relativi contenuti non vengono parsati (chiuso issue #8)
 * La divisione delle immagini ora è più intelligente: eventuali sotto-immagini con rapporti ridicoli vengono ignorate
 * Migliorata gestione errori in elaborazione illustrazioni - Ad esempio se in un artcolo c'è un immagine JPEG (caso rarissimo) viene correttamente saltata

# 2.2.7 (06/02/2018)
 * Migliorati script di setup e manutenzione

# 2.2.6 (13/01/2018)
 * Se gli articoli non hanno i paragrafi numerati i contenuti vengono ordinati insemplice ordine progressivo (evitando così di avere tutti i file che iniziano con 'Par. 00')

# 2.2.5 (07/01/2018)
 * Bugfix: a causa di un typo in zippatore.js i file non venivano ordinati per paragrafo/parte
 * Iniziato versioning numerico standard (major.minor.patch), changelog aggiornato retroattivamente
 * Migliorato script di aggiornamento - Ora vengono aggiornati e installati i font usati dall'applicazione

# 2.2.4 (30/12/2017)
 * Creati script per installazione e riavvio
 * Migliorato script aggiornamento
 * Bugfix: se tra i link trovati c'era un link "mediator" per un contenuto non disponibile nella lingua si verificava un'errore interno; ora il caso viene gestito

# 2.2.3 (27/12/2017)
 * Migliorata nomenclatura file in creaArchivio
 * Migliorato comportamento nel caso le immagini abbiano già lo stesso rapporto del canvas

# 2.2.2 (20/12/2017)
 * Migliorato blur su sfondo immagini: ora vengono compaiono meno "aloni" bianchi
 * Migliorata divisione immagini: ora le divisioni verticali vengono cercate su una delle ultime righe anziché sulla prima (che talvolta è completamente bianca per le sotto-immagini)

# 2.2.1 (18/12/2017)
 * Ora le parti di *Vita cristiana* vengono correttamente identificate anche se su *mwb* ci sono dei sotto-elenchi.

# 2.2.0 (06/12/2017)
 * Divisione delle illustrazioni composte tramite il nuovo parametro per l'elaborazione: dividiImmaginiComposte

# 2.1.1 (05/12/2017)
 * Migliore interpretazione degli URL "mediator"
 * Nuovo parametro per l'elaborazione: lunghezzaMaxVideo

# 2.1.0 (04/12/2017)
 * Migliore individuazione dei paragrafi a cui le illustrazioni sono legate
 * Nuovo endpoint /screenshot che restituisce lo screenshot di una pagina web

# 2.0.0 (29/11/2017)
 * Overhaul generale del codice
 * Trasferito parte del codice in moduli npm separati (`canvas-multiline-text` e `words-array`)
 * Migliore algoritmo di scelta del colore modale di sfondo
 * Se l'intervallo di paragrafi (#h) parte da 1 prende anche l'immagine iniziale

# 1.3.3 (09/10/2017)
 * Ora viene usato il modulo greenlock-express-wrapper

# 1.3.2 (04/10/2017)
 * Supporto SSL
 * Se con la richiesta "/" non viene inviato nessun dato viene mostrato un link al repository

# 1.3.1 (17/07/2017)
 * Bugfix POST /upload
 * Ora vengono elaborate meglio anche le immagini in jw.org

# 1.3.0 (21/04/2017)
 * Aggiunta richiesta POST /upload, che permette di elaborare immagini caricate dall'utente

# 1.2.1 (11/03/2017)
 * Il riferimento dei versetti viene stampato più in piccolo se è troppo lungo.
 * Non vengono più restituiti contenuti duplicati (es.: video che vengono scaricati due volte perché sono linkati sia nel programma della settimana che nell'articolo specifico).

# 1.2.0 (21/02/2017)
 * Aggiunta richiesta /cors, utilizzata dal frontend web.

# 1.1.0 (12/02/2017)
 * Gli elementi non vengono elaborati più di una volta (vedi illustrazioni in riquadri SBC)
 * Font per cinese

# 1.0.0 (01/01/2017)
 * Prima release in Node.JS
