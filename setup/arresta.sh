#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Avviami come root"
  exit
fi

cd ~/isbobackend
forever stopall
