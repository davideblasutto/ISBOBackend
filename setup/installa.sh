#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Avviami come root"
  exit
fi

# Installa Node.js
cd ~
curl -sL https://deb.nodesource.com/setup_9.x -o nodesource_setup.sh
bash nodesource_setup.sh
apt-get -y install nodejs
apt-get -y install build-essential

# Dipendenze per canvas e gm
apt-get -y install libcairo2-dev libjpeg-dev libpango1.0-dev libgif-dev build-essential g++
apt-get -y install graphicsmagick

# Installa MySQL
apt-get install mysql-server
cd ~/isbobackend/setup
mysql -u root -p123 < database.sql

# Installa forever
npm i -g forever

# Alias per script aggiorna.sh, riavvia.sh e arresta.sh
echo "" >> ~/.bashrc
echo "# Alias ISBOBackend" >> ~/.bashrc
echo "alias riavvia=\"sudo ~/isbobackend/setup/riavvia.sh\"" >> ~/.bashrc
echo "alias aggiorna=\"sudo ~/isbobackend/setup/aggiorna.sh\"" >> ~/.bashrc
echo "alias arresta=\"sudo ~/isbobackend/setup/arresta.sh\"" >> ~/.bashrc

# Riavvia il server una volta al giorno
(crontab -l 2>/dev/null; echo "@daily sudo /sbin/shutdown -r now") | crontab -

# Avvia ISBOBackend all'avvio del sistema
(crontab -l 2>/dev/null; echo "@reboot sudo ~/isbobackend/setup/riavvia.sh") | crontab -

# Installa ISBOBackend
cd ~/isbobackend/setup
chmod +x aggiorna.sh
bash aggiorna.sh
