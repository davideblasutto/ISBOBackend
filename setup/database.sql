-- Dump della struttura del database isbo
CREATE DATABASE `isbo`;
USE `isbo`;

-- Dump della struttura di tabella isbo.richiesta
CREATE TABLE `Richiesta` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IP` varchar(32) COLLATE utf8_bin NOT NULL,
  `TimestampInizio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Metodo` varchar(8) COLLATE utf8_bin NOT NULL,
  `Endpoint` varchar(16) COLLATE utf8_bin NOT NULL,
  `Parametri` text COLLATE utf8_bin,
  `Esito` bit(1) DEFAULT b'0',
  `TimestampFine` timestamp NULL DEFAULT NULL,
  `Errore` text COLLATE utf8_bin,
  `Metadati` text COLLATE utf8_bin,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dump della struttura di vista isbo.richiestaintegrata
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `RichiestaIntegrata` AS SELECT
	*,
	TIMESTAMPDIFF(SECOND, TimestampInizio, TimestampFine) AS Durata
FROM Richiesta
WHERE Endpoint = '/' OR Endpoint = '/elabora'
ORDER BY ID DESC ;

-- Dump della struttura di vista isbo.richiestafallita
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `RichiestaFallita` AS SELECT * FROM RichiestaIntegrata
WHERE Esito != 1
ORDER BY ID DESC ;

-- Accesso dall'esterno
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'pwdISBO2018!&~';
