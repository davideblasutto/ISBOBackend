#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Avviami come root"
  exit
fi

# Arresta il servizio
forever stopall

# Aggiorna codice sorgente
cd ~/isbobackend
git stash
git pull

# Rende questi script eseguibili
chmod +x setup/aggiorna.sh
chmod +x setup/riavvia.sh
chmod +x setup/installa.sh
chmod +x setup/arresta.sh

# Aggiorna font
mkdir ~/.fonts
cp .fonts/* ~/.fonts
fc-cache -fv

# Aggiorna pacchetti npm
sudo npm i
forever start index.js
