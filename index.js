//
// ## Configurazione del server
//

// ## 1. Carica subito alcuni moduli di utilty varie

// Costanti
global.costanti = require('./costanti.js')

// Filesystem
global.path = require('path')
global.fs = require('fs')

// Node-Canvas
global.Canvas = require('canvas')
global.Image = require('canvas').Image
global.drawMultilineText = require('canvas-multiline-text')

// Utility varie
global.utils = require('./utils.js')
global.rnd = require('randomstring')
global.request = require('request')
global.gm = require('gm')
global.requestPromise = require('request-promise-native')
global.cheerio = require('cheerio')
global.isEmptyObject = require('is-empty-object')
global.fileType = require('file-type')
global.StackBlur = require('stackblur-canvas')
global.deparam = require('jquery-deparam')
global.wordsArray = require('words-array')
global.util = require('util')
global.webshot = require('webshot')
global.getVideoInfo = require('get-video-info-url')
global.writePromise = require('fs-writefile-promise/lib/node7')
global.mysql = require('mysql')
global.CircularJSON = require('circular-json')

// Oggetti jQuery globale, da usare solo per funzioni tipo $(this) o $.forEach...
// Per manipolare DOM veri e propri istanziare un nuovo oggetto con cheerio
global.$ = cheerio.load('<span>Hellow</span>')

// Moduli applicazione (alcuni sono classi, altri semplici moduli)
global.WolParser = require('./isbo/wol-parser.js')
global.Elaboratore = require('./isbo/elabora-contenuti.js')
global.zippatore = require('./isbo/zippatore.js')

// Logger
const Winston = require('winston')
global.logger = new Winston.Logger({
	transports: [
		new Winston.transports.Console({
			formatter: function(options) {

				// Il livello 9 dello stack di esecuzione è il primo a non essere legato al log di per sé
				var prefisso = path.basename(utils.traceCaller(9)) + ' - '

				// Testo semplice o oggetto?
				var testo = (isEmptyObject(options.meta) ? options.message : util.inspect(options.meta))
				// Indentazione
				testo = testo.replace(/\n/g, '\n' + ' '.repeat(prefisso.length))

				return prefisso + testo

			}
		})
	]
})

// Log su MySQL
global.connessioneMySQL = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '123',
	database: 'isbo'
})
// La connessione è asincrona, ma ora della prima richiesta dovrebbe aver finito
connessioneMySQL.connect(function(err) {
	if (err) {
		logger.info('Errore nella connessione a MySQL:')
		logger.info(err)
		logger.info('L\'errore non è bloccante, il server verrà avviato senza logging permanente')
		// Annulla anche l'oggeto global.connessioneMySQL
		connessioneMySQL = null
	} else
		logger.info('Connesso a MySQL')
})


// ## 2. Crea e configura il server

// Server Express
const server = require('express')()
// Abilita CORS
var cors = require('cors')
server.use(cors())
// Usa express-fileupload
server.use(require('express-fileupload')())

// Middleware personalizzato
server.use(require('./middleware.js'))

// ## 3. Operazioni legate al filesystem

// Percorso root
global.cartellaRoot = path.dirname(require.main.filename)

// Crea cartella tmp se non c'è
if (fs.readdirSync(cartellaRoot).indexOf('tmp') == -1)
	fs.mkdir(path.join(cartellaRoot, 'tmp'))

// Carica immagine di sfondo per le scritture
// Caricamento asicrono, ora della prima richiesta dovrebbe comunque aver finito
fs.readFile(path.join(cartellaRoot, 'var/sfondo-jwb.png'), async function(err, buffer){

	global.sfondo = await buffer.toImage()
	logger.info('Immagine di sfondo per le scriture caricata: [' + sfondo.width + 'x' + sfondo.height + ']')

})

// ## 4. Endpoint del server

// "/" e "/elabora" sono due alias dello stesso endpoint
server.get('/', require('./endpoint/elabora.js'))
server.get('/elabora', require('./endpoint/elabora.js'))
server.post('/upload', require('./endpoint/upload.js'))
server.get('/versione', require('./endpoint/versione.js'))
server.get('/cors', require('./endpoint/cors.js'))
server.get('/screenshot', require('./endpoint/screenshot.js'))

//
// ## 5. Avvio del server
//
require('greenlock-express-wrapper').listen({
	express: server,
	email: 'davide.blasutto@gmail.com',
	approveDomain: [ 'isbo.mooo.com', 'isbo.info.tm' ],
	plainPort: 8080,
	verbose: true,
	logFunction: function(messaggio) { logger.info(messaggio) }
})

// Gestitsce promise rifiutate e non gestite
process.on('unhandledRejection', (reason, p) => {
	console.log('Rejection non gestita: ', p)
})
