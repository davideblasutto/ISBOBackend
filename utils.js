// Funzioni varie


//
// ## Se necessario converte un link dal formato "datalink" a "finder"
// ## Es http://wol.jw.org/en/wol/datalink/r1/lp-e?docid=1001071946 diventa http://www.jw.org/finder?docID=1001071946&wtlocale=E
//
exports.convertiUrlEsterno = function(url) {

	if (url.indexOf('finder?') != -1 || url.indexOf('tv.jw.org') != -1)
		// Va già bene
		return url

	// Scompone l'url
	var pagina = url.substr(0, url.indexOf('?'))
	parametri = deparam(url.substr(url.indexOf('?') + 1))

	// 1. docID
	var docID = parametri.docid

	// 2. locale
	var locale = pagina.substr(pagina.lastIndexOf('lp-') + 3).toUpperCase()

	// Ricompone l'url
	url = 'http://www.jw.org/finder?docID=' + docID + '&wtlocale=' + locale

	logger.info('URL esterno convertito: ' + url)
	return url

}

//
// ## Genera un oggetto Image (node-canvas) da un buffer, se possibile
//
Buffer.prototype.toImage = async function() {

	return new Promise((resolve, reject) => {

		try {

			// Controlla il mime type del buffer
			const mimeTypeBuffer = fileType(this).mime
			logger.info('Creo Image da buffer con mime type ' + mimeTypeBuffer)

			if (!costanti.mimetypeValidi.includes(mimeTypeBuffer))
				reject('Mime type ' + mimeTypeBuffer + ' non valido per Image')

			var base64 = this.toString('base64')
			var image = new Image()
			image.onload = () => resolve(image)
			image.onerror = (errore) => reject(errore)
			image.src = 'data:' + mimeTypeBuffer + ';base64,' + base64

		} catch (errore) {

			reject(errore)

		}

	})

}

//
// ## Restituisce un oggetto Image contenente l'immagine corrispondente all'url (HTTP o locale), in formato PNG
//
exports.getPngDaUrl = function(url) {

	return new Promise((resolve, reject) => {

		var origine
		if (url.indexOf('http') === 0)
			origine = request(url)
		else
			origine = url

		const percorsoLocaleImg = path.join(cartellaRoot, 'tmp/img-' + rnd.generate(10) + '.png')
		gm(origine).write(percorsoLocaleImg, function(errore) {
			if (errore)
				reject(errore)
			else
				fs.readFile(percorsoLocaleImg, function(errore, dati){
					if (errore)
						reject(errore)
					else {
						var base64Image = new Buffer(dati, 'binary').toString('base64')
						var img = new Image
						img.onload = function() {
							fs.unlink(percorsoLocaleImg, function() { /* Evita DeprecationWarning */ })
							resolve(img)
						}
						img.src = 'data:image/png;base64,' + base64Image
					}

				})

		})

	})

}

//
// ## Restituisce il canvas con l'immagine croppata
//
Image.prototype.canvasCroppato = function(area) {

	var canvas = new Canvas(area.larghezza, area.altezza)
	var ctx = canvas.getContext('2d')

	ctx.drawImage(
		// Immagine
		this,
		// Crop
		area.x, area.y, area.larghezza, area.altezza,
		// Dove disegnare nel canvas ( = intera superficie del canvas)
		0, 0, canvas.width, canvas.height
	)

	return canvas

}


//
// ## Restituisce il canvas con la porzione (frazione di 1) richiesta dell'immagine, sul margine indicato
//
Image.prototype.estraiCanvasMargine = function(margine, porzione) {

	// Calcola "h" o "w"
	var h = 0, w = 0
	if (margine == 'sopra' || margine == 'sotto')
		h = parseInt(this.height * porzione)
	else
		w = parseInt(this.width * porzione)

	switch (margine) {

	case 'sopra':
		return this.canvasCroppato({
			x: 0, y: 0,
			larghezza: this.width, altezza: h
		})

	case 'sotto':
		return this.canvasCroppato({
			x: 0, y: this.height - h,
			larghezza: this.width, altezza: h
		})

	case 'sinistra':
		return this.canvasCroppato({
			x: 0, y: 0,
			larghezza: w, altezza: this.height
		})

	case 'destra':
		return this.canvasCroppato({
			x: this.width - w, y: 0,
			larghezza: w, altezza: this.height
		})

	}

}

//
// ## Restituisce il colore più frequente in un canvas
//
Canvas.prototype.coloreModale = function(soglia) {

	// Se per qualche ragione l'immagine è alta/larga 0 pixel non la processa
	if (this.width == 0 || this.height == 0)
		return null

	// Soglia di default
	if (!soglia)
		soglia = .8

	// imageData contiene 4 elementi per pixel (A, B, G, R)
	var imageData = this.getContext('2d').getImageData(0, 0, this.width, this.height).data

	// Estrae i pixel dall'immagine
	// Per rendere il tutto più veloce non li converte in nessun modo, restano valori interi tipo 4294967295 (bianco)
	var arrayPixel = new Uint32Array(imageData.buffer)

	// Calcola la frequenza dei vari colori
	var datiModa =  exports.modeArray(arrayPixel)

	// datiModa contiene due elementi:
	//  - occorrenze: oggetto con le occorrenze di tutti i valori trovati, dove il valore è il nome della proprietà e il numero di occorrenze è il valore
	//  - mode: valori ( = colori) con il maggior numero di occorrenze
	// Immaginando le occorrenze come un grafico, se ha una struttura simil-gaussiana vuol dire che c'è un colore modale
	// In pratica controlla se le occorrenze della moda e dei suoi colori vicini superano una certa percentuale del totale
	var chiavi = Object.keys(datiModa.occorrenze)
	var ampiezza = chiavi[chiavi.length - 1] - chiavi[0]
	var scarto = 0.5 // Larghezza del picco rispetto all'intera ampiezza di valori
	// Calcola valori di inizio e fine del picco
	var inizioPicco = datiModa.mode[0] - ampiezza * (scarto / 2)
	var finePicco = datiModa.mode[0] + ampiezza * (scarto / 2)
	var occorrenze = 0
	for (var valore in datiModa.occorrenze)
		// Per ogni valore testa se è sufficientemente vicino alla moda
		if (valore >= inizioPicco && valore <= finePicco)
			occorrenze += datiModa.occorrenze[valore]

	var abgr = datiModa.mode[0].toString(16) // L'ordine dei colori è invertito
	var colorePiuDiffuso = '#' +  abgr.substr(6, 2) + abgr.substr(4, 2) + abgr.substr(2, 2)
	logger.info('Colore più diffuso: ' + colorePiuDiffuso + ' - Occorrenze: ' + occorrenze + ' su ' + arrayPixel.length + ' (' + parseInt(occorrenze / arrayPixel.length * 100) + '%)')

	// Se il colore più diffuso copre la percentuale di soglia dell'immagine allora è "modale"
	if ((occorrenze / arrayPixel.length) >= soglia) {
		logger.info('Il colore supera la soglia del ' + parseInt(soglia * 100) + '%')
		return colorePiuDiffuso
	} else {
		logger.info('Il colore *non* supera la soglia del ' + parseInt(soglia * 100) + '%')
		return null
	}

}

//
// ## Recupera il nome e la riga dello script nell'ennesimo livello dello stack di esecuzione
// ## https://stackoverflow.com/questions/13410754/i-want-to-display-the-file-name-in-the-log-statement
//
exports.traceCaller = function(livello) {
	if( isNaN(livello) || livello<0) livello=1
	livello+=1
	var s = (new Error()).stack
	var a=s.indexOf('\n',5)
	while(livello--) {
		a=s.indexOf('\n',a+1)
		if( a<0 ) { a=s.lastIndexOf('\n',s.length); break}
	}
	b=s.indexOf('\n',a+1); if( b<0 ) b=s.length
	a=Math.max(s.lastIndexOf(' ',b), s.lastIndexOf('/',b))
	b=s.lastIndexOf(':',b)
	s=s.substring(a+1,b)
	return s
}

// ##
// ## Converte un'array di stringhe in un oggetto con valori bool, in base alla presenza nell'array originale
// ## Es.: [ "link", "illustrazioni" ] => { link: true, illustrazioni: true, riferimenti: false, video: false }
//
exports.convertiArrayInOggettoConFlag = function(arrayOriginale, arrayPossibiliValori) {

	var oggetto = {}

	arrayPossibiliValori.forEach(valore => {

		// Per ogni possibile valore presente nell'array
		oggetto[valore] = (arrayOriginale.indexOf(valore) != -1)

	})

	return oggetto

}

//
// ## Restituisce l'estensione di un file (anche se con percorso completo o in un URL)
//
String.prototype.estensioneFile = function() {
	return this.substr(this.lastIndexOf('.') + 1)
}

//
// ## Rimuove l'estensione di un file
//
String.prototype.rimuoviEstensione = function() {
	return this.substr(0, this.lastIndexOf('.'))
}

//
// ## String.prototype.capitalizeFirstLetter
//
String.prototype.capitalizeFirstLetter = function() {
	return this.charAt(0).toUpperCase() + this.slice(1)
}

//
// ## Wrapper di request-promise + cheerio per la richiesta di pagine HTML
// ## Restituisce { $, rispostaCompleta } oppure null in caso di errori HTTP
//
exports.richiestaHtml = async function (uri) {

	logger.info('[HTTP GET] ' + uri)

	return requestPromise({
		uri: encodeURI(uri),
		resolveWithFullResponse: true
	}).then(risposta => {

		return {
			jQuery: cheerio.load(risposta.body),
			oggetto: risposta
		}

	}).catch(err => {

		if (err.response)
			logger.info('Errore HTTP: ' + err.response.statusCode)
		else
			logger.info('Errore HTTP: ' + err.message)

		return null

	})

}

//
// ## Converte un selettore di "a" in un'array dei releativi attributi arrayHref
//
exports.arrayHref = function ($selettore) {
	return $selettore.map(function(){
		return $(this).attr('href')
	}).get()
}

//
// ## Sanitizza una stringa per usarla come nome file
//
String.prototype.sanitizzaStringaPerNomeFile = function(){
	var str = this.replace(':', ' ')
	return (str)
}

//
// ## Trova la moda in un'array di valori
//
// http://stackoverflow.com/questions/1053843/get-the-element-with-the-highest-occurrence-in-an-array
exports.modeArray = function(array) {
	if (array.length == 0)
		return null
	var modeMapObj = {},
		maxCount = 1,
		modes = [array[0]]

	for(var i = 0; i < array.length; i++)
	{
		var el = array[i]

		if (modeMapObj[el] == null) {
			modeMapObj[el] = 1
		} else {
			modeMapObj[el]++
		}

		if (modeMapObj[el] > maxCount)
		{
			modes = [el]
			maxCount = modeMapObj[el]
		}
		else if (modeMapObj[el] == maxCount)
		{
			modes.push(el)
			maxCount = modeMapObj[el]
		}
	}
	return { mode: modes, occorrenze: modeMapObj }
}
