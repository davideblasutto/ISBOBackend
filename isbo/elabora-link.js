// Questo modulo si occupa di creare e restituire i contenuti di tipo "link" e "video"

const Elaboratore = require('./elabora-contenuti.js')

//
// ## Elabora un link
// ## Formato dell'oggetto "link":
// ## { tipo: 'link', titolo: (stringa), url: (stringa) }
// ## Restituisce l'oggetto link con in più le proprietà
// ##  - fileUrl: contenuto del file .url corrispondente
// ##  - urlDirettoFile: eventuale link diretto al file multimediale
//
Elaboratore.prototype.elaboraLink = async function(link) {

	logger.info('Elaboro link: ' + link.url)

	// Converte l'url, se necessario
	link.url = utils.convertiUrlEsterno(link.url)

	const rispostaPaginaLink = await utils.richiestaHtml(link.url)

	// Se non è stato possibile risolvere l'url restiuscie l'oggetto link così come è stato ricevuto
	if (!rispostaPaginaLink)
		return link

	var $paginaLink = rispostaPaginaLink.jQuery

	// Se l'url conteneva "mediator" vuol dire che è un video di JW Broadcasting
	if (link.url.indexOf('mediator') != -1) {

		// tv.jw.og
		logger.info('Tipo link: video su JW Broadcasting')

		// Converte il link da https://mediator.jw.org/finder?item={elemento}&lang={lingua} a https://mediator.jw.org/v1/media-items/{lingua}/{elemento}
		const campiUrl = deparam(link.url.substr(link.url.indexOf('?') + 1).replace(';', '&'))
		const urlMediator = 'https://mediator.jw.org/v1/media-items/' +  campiUrl.lang + '/' + campiUrl.item
		logger.info('URL mediator: ' + urlMediator)

		// Ottiene i dati dall'url mediator
		const datiMediator = JSON.parse(await requestPromise(urlMediator))

		// Cerca il link al file nella risoluzione adatta (se presente)
		if (datiMediator.media.length)
			for(file of datiMediator.media[0].files) {
				if (file.label == ('' + this.richiesta.parametri.risoluzioneVideo + 'p'))
					// Salva l'url diretto al file
					link.urlDirettoFile = file.progressiveDownloadURL
			}

	} else {

		// www.jw.org

		// Cerca di capire se è un file scaricabile o un articolo
		const urlIncludeVideo = $($paginaLink('.jsIncludeVideo').text()).find('a').attr('href')
		logger.info('URL includeVideo: ' + urlIncludeVideo)

		if (urlIncludeVideo) {

			// E' un video
			logger.info('Tipo link: file su JW.ORG')

			// Richiede il contenuto della pagina "includeVideo" e cerca al suo interno il link al file
			const rispostaIncludeVideo = await utils.richiestaHtml(urlIncludeVideo)
			var $paginaIncludeVideo = rispostaIncludeVideo.jQuery

			// Salva l'url diretto al file
			link.urlDirettoFile = $paginaIncludeVideo('a[href*=' + this.richiesta.parametri.risoluzioneVideo + ']').first().attr('href')

		} else {

			// E' un articolo
			logger.info('Tipo link: altro (non è un file scaricabile direttamente)')

		}

	}

	// Ignora l'eventuale url diretto se il video corrispondente è troppo lungo
	// L'estrazione dei metadati del file video può richiedere un po' quindi lo faccio solo se si sta davvero cercando anche i video
	if (link.urlDirettoFile && this.richiesta.parametri.cerca.video) {
		const metadatiVideo = await getVideoInfo(link.urlDirettoFile)
		logger.info('Duranta video: ' + parseInt(metadatiVideo.format.duration / 60) + ' minuti')
		if (metadatiVideo.format.duration >= this.richiesta.parametri.lunghezzaMaxVideo * 60) {
			logger.info('Video troppo lungo, non lo scarico')
			delete link.urlDirettoFile
		}
	}

	// Indipendentemente dal tipo di link crea il contenuto del file .URL da aggiungere in seguito all'archivio
	link.fileUrl = '[InternetShortcut]\r\nURL=' + link.url

	return link

}
