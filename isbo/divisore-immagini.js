// Questo modulo si occupa di cercare di dividere una singola immagine in più
// sotto-immagini, sulla base di eventuali aree bianche di divisione
// Utilizzo:
//	const DivisoreImmagini = require('./divisore-immagini.js')
//	const sottoImmagini = await (new DivisoreImmagini(illustrazione, immagine, elaboratore)).dividi()

//
// ## Costruttore della classe DivisoreImmagini
//
var DivisoreImmagini = function(metadatiIllustrazione, immagine, elaboratore) {

	this.metadatiIllustrazione = metadatiIllustrazione
	this.imgPrincipale = immagine
	this.elaboratore = elaboratore

}

//
// ## Se this.imgPrincipale è divisibile restituisce un array di contenuti,
// ## corrispondenti alle "sotto-immagini"
//
DivisoreImmagini.prototype.dividi = async function() {

	// Massimo rapporto (orizzontale o verticale) consentito per una sottoimmagine
	var rapportoMassimoConsentito = 2.5

	// Crea un canvas temporaneo per ottenere l'imageData
	const canvas = new Canvas(this.imgPrincipale.width, this.imgPrincipale.height)
	const ctx = canvas.getContext('2d')
	ctx.drawImage(this.imgPrincipale, 0, 0)

	// imageData contiene 4 elementi per pixel (A, B, G, R)
	const imageData = ctx.getImageData(0, 0, this.imgPrincipale.width, this.imgPrincipale.height).data

	// Incrementa il contrasto
	const contrast = 35
	var factor = (259 * (contrast + 255)) / (255 * (259 - contrast))
	for(var i=0;i<imageData.length;i+=4) {
		imageData[i] = factor * (imageData[i] - 128) + 128
		imageData[i+1] = factor * (imageData[i+1] - 128) + 128
		imageData[i+2] = factor * (imageData[i+2] - 128) + 128
	}

	// Estrae i pixel dall'immagine
	// Per rendere il tutto più veloce non li converte in nessun modo, restano valori interi tipo 4294967295 (bianco)
	this.arrayPixel = new Uint32Array(imageData.buffer)

	var inizioZonaInterruzione = null
	var sottoImmagini = []

	// Inizia a cercare interruzioni verticali
	logger.info('Cerco interruzioni verticali...')
	inizioZonaInterruzione = null
	var xInizioSottoImmagine = 0
	// Testa la quinta riga dal fondo - Solitamente è la più adatta
	const yBase = this.imgPrincipale.width * this.imgPrincipale.height - (this.imgPrincipale.width * 5)
	for (var i = 0; i < this.imgPrincipale.width; i++) {

		// Per ogni pixel della prima riga
		if (this.arrayPixel[yBase + i] == costanti.bianco && !inizioZonaInterruzione) {

			// È il primo pixel bianco: inizia una "zona di interruzione"
			inizioZonaInterruzione = i

		} else if (this.arrayPixel[yBase + i] != costanti.bianco && inizioZonaInterruzione) {

			// È il primo pixel non-bianco: termina una "zona di interruzione"
			const fineZonaInterruzione = i - 1
			const puntoMedianoZonaInterruzione = parseInt(inizioZonaInterruzione + ((fineZonaInterruzione - inizioZonaInterruzione) / 2))

			// Ignora la zona di interruzione se non parte dall'inizio della sotto-immagine
			if (inizioZonaInterruzione >= xInizioSottoImmagine + this.imgPrincipale.width * 0.1) {

				// Calcola dimensioni sotto-immagine
				var larghezzaSottoImmagine = inizioZonaInterruzione - xInizioSottoImmagine - 2
				var altezzaSottoImmagine = this.imgPrincipale.height

				// Controlla se prosegue su tutta l'altezza dell'immagine
				if (this.interaColonnaBianca(puntoMedianoZonaInterruzione) && (altezzaSottoImmagine / larghezzaSottoImmagine < rapportoMassimoConsentito)) {

					logger.info('Trovata interruzione verticale alla colonna di pixel ' + puntoMedianoZonaInterruzione + ' (' + parseInt(puntoMedianoZonaInterruzione / this.imgPrincipale.width * 100) + '%)')
					// Memorizza il rettangolo della sotto-immagine trovata
					sottoImmagini.push({
						x: xInizioSottoImmagine,
						y: 0,
						larghezza: larghezzaSottoImmagine,
						altezza: altezzaSottoImmagine
					})
					// Imposta l'inizio della prossima sotto-immagine
					xInizioSottoImmagine = fineZonaInterruzione + 2

				} else {

					// Continua a cercare una nuova "zona"
					inizioZonaInterruzione = null

				}

			} else {

				// Continua a cercare una nuova "zona"
				inizioZonaInterruzione = null

			}

		}

	}
	// Se è stata trovata almeno una sotto-immagine allora "chiude" l'ultima
	if (sottoImmagini.length)
		sottoImmagini.push({
			x: xInizioSottoImmagine,
			y: 0,
			larghezza: this.imgPrincipale.width - xInizioSottoImmagine,
			altezza: this.imgPrincipale.height
		})

	// Se non ha già trovato delle sotto-immagini cerca interruzioni orizzontali
	if (!sottoImmagini.length) {

		logger.info('Cerco interruzioni orizzontali...')
		inizioZonaInterruzione = null
		var yInizioSottoImmagine = 0
		for (var i = 0; i < this.imgPrincipale.height * this.imgPrincipale.width; i += (this.imgPrincipale.width + 1)) {

			// Per ogni pixel della prima colonna
			if (this.arrayPixel[i] == costanti.bianco && !inizioZonaInterruzione) {

				// È il primo pixel bianco dopo dei pixel non-bianchi: inizia una "zona di interruzione"
				inizioZonaInterruzione = i / (this.imgPrincipale.width + 1)

			} else if (this.arrayPixel[i] != costanti.bianco && inizioZonaInterruzione) {

				// È il primo pixel non-bianco: termina una "zona di interruzione"

				const fineZonaInterruzione = i / (this.imgPrincipale.width + 1) - 1
				const puntoMedianoZonaInterruzione = parseInt(inizioZonaInterruzione + ((fineZonaInterruzione - inizioZonaInterruzione) / 2))

				// Ignora la zona di interruzione se non parte dall'inizio della sotto-immagine
				if (inizioZonaInterruzione >= yInizioSottoImmagine + this.imgPrincipale.height * 0.1) {

					// Calcola larghezza e altezza della sotto-immagine
					var larghezzaSottoImmagine = this.imgPrincipale.width
					var altezzaSottoImmagine = inizioZonaInterruzione - yInizioSottoImmagine - 2

					// Considera la sotto-immagine valida se:
					//	- L'interruzione prosegue su tutta la larghezza dell'immagine
					//  - Il rapporto della sottoimmagine non è troppo stretto
					if (this.interaRigaBianca(puntoMedianoZonaInterruzione) && (larghezzaSottoImmagine / altezzaSottoImmagine < rapportoMassimoConsentito)) {

						logger.info('Trovata interruzione orizzontale alla riga di pixel ' + puntoMedianoZonaInterruzione + ' (' + parseInt(puntoMedianoZonaInterruzione / this.imgPrincipale.height * 100) + '%)')
						// Memorizza il rettangolo della sotto-immagine trovata
						sottoImmagini.push({
							x: 0,
							y: yInizioSottoImmagine,
							larghezza: larghezzaSottoImmagine,
							altezza: altezzaSottoImmagine,
						})
						// Imposta l'inizio della prossima sotto-immagine
						yInizioSottoImmagine = fineZonaInterruzione + 2

					} else {

						// Continua a cercare una nuova "zona"
						inizioZonaInterruzione = null

					}

				} else {

					// Continua a cercare una nuova "zona"
					inizioZonaInterruzione = null

				}

			}

		}
		// Se è stata trovata almeno una sotto-immagine allora "chiude" l'ultima
		if (sottoImmagini.length)
			sottoImmagini.push({
				x: 0,
				y: yInizioSottoImmagine,
				larghezza: this.imgPrincipale.width,
				altezza: this.imgPrincipale.height - yInizioSottoImmagine
			})

	}

	// Questo è ciò che viene restituito
	var contenuti = []

	// Sotto-immagini trovate?
	if (sottoImmagini.length) {

		logger.info('Trovate sotto-immagini:')
		logger.info(sottoImmagini)

		var metadati = []

		// Splitta il titolo dell'immagine e cerca di usarlo come titoli per le sotto-immagini
		const titoloSplittato = this.metadatiIllustrazione.titolo.split(';')

		var i = 0
		for (var areaSottoImmagine of sottoImmagini) {
			i++

			// Per ogni sotto-immagine:

			// Crea un canvas temporaneo per ottenere l'imageData
			const canvasSottoImmagine = this.imgPrincipale.canvasCroppato(areaSottoImmagine)

			// Salva su file
			const percorsoLocaleImg = await writePromise(
				path.join(cartellaRoot, 'tmp/subimg-' + rnd.generate(10) + '.png'),
				canvasSottoImmagine.toBuffer()
			)

			var titolo = this.metadatiIllustrazione.titolo
			if (titoloSplittato.length == sottoImmagini.length)
				titolo = titoloSplittato[i - 1]

			metadati.push({
				tipo: 'illustrazione',
				locale: true,
				paragrafo: this.metadatiIllustrazione.paragrafo,
				ordine: this.metadatiIllustrazione.ordine,
				titolo: '[' + i + '] ' + titolo,
				src: percorsoLocaleImg
			})

		}

		// Avvia in parallelo tutte le funzioni di elaborazione
		const elaborazioni = metadati.map(elemento => {
			return this.elaboratore.elaboraIllustrazione(elemento)
		})

		logger.info('Avvio l\'elaborazione delle sotto-imagini...')
		for (const elaborazione of elaborazioni) {
			const contenuto = await elaborazione
			// Se l'esito dell'elaborazione è null non lo aggiunge all'array
			if (contenuto)
				contenuti = contenuti.concat(contenuto)
		}

	}

	// Quando sono tutti elaborati restituisce l'array dei contenuti
	return contenuti

}

//
// ## Restituisce true se l'intera colonna x di this.arrayPixel è bianca
//
DivisoreImmagini.prototype.interaColonnaBianca = function(x) {

	logger.info('Verifico colore pixel su colonna ' + x + '...')
	// Testa un pixel ogni 5 nella colonna
	for (var i = 1; i < this.imgPrincipale.height; i = i + 5) {

		if (this.arrayPixel[this.imgPrincipale.width * i + x] != costanti.bianco)
			return false

	}

	return true

}

//
// ## Restituisce true se l'intera riga y di this.arrayPixel è bianca
//
DivisoreImmagini.prototype.interaRigaBianca = function(y) {

	logger.info('Verifico colore pixel su riga ' + y + '...')
	// Testa un pixel ogni 5 nella riga
	for (var i = y * this.imgPrincipale.width; i < (y * this.imgPrincipale.width) + this.imgPrincipale.width; i = i + 5) {

		if (this.arrayPixel[i] != costanti.bianco)
			return false

	}

	return true

}

module.exports = DivisoreImmagini
