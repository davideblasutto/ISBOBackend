// Questo modulo si occupa di recuperare tutti i metadati necessari per creare i contenuti,
// mettendo a disposizione la classe WolParser
// Prevede che la richiesta ricevuta come argomento contenga l'oggetto "parametri"
// Utilizzo:
//	const WolParser = require('./wol-parser.js')
//	const metadati = await new WolParser(richiestaExpress).ottieniMetadati()

//
// ## Costruttore della classe WolParser
//
var WolParser = function(richiesta) {

	// Oggetto richiesta di express disponibile ovunque nell'oggetto WolParser
	// Serve per accedere a richiesta.parametri
	this.richiesta = richiesta

}

//
// ## Elabora una richiesta (di qualsiasi tipo tra quelle possibili) e restituisce un oggetto con queste proprietà:
// ##  - elementi: array con i metadati necessari a creare/scaricare i contenuti
// ##  - urlHome: serve per le elaborazioni in seguito
//
WolParser.prototype.ottieniMetadati = async function() {

	// this.richiesta.parametri.origine.valore può essere (in base a this.richiesta.parametri.origine.tipo):
	//  1. Un riferimento: 'Matteo 24:14'
	//  2. Un URL di un articolo: 'http://wol.jw.org/it/wol/d/r6/lp-i/110201424'
	//  3. Un adunanza: [ vcm | w | sbc ]

	logger.info('Ottengo i metadati per la richiesta:')
	logger.info(this.richiesta.parametri)

	// Tutto finirà qui dentro
	var metadati = { urlHome: '', elementi: [] }

	// Prima di tutto va sulla home page di wol per determinare il link da usare per le ricerche delle scritture
	const rispostaHome = await utils.richiestaHtml(costanti.urlBaseWol + this.richiesta.parametri.lingua)
	// Se non riesce ad accedere alla home blocca l'elaborazione
	if (!rispostaHome)
		throw 'Impossibile accedere alla home della Biblioteca Online'

	var $home = rispostaHome.jQuery

	// L'url della home servirà in seguito, quindi lo salva
	metadati.urlHome = costanti.urlBaseWol + $home('#siteBanner a').attr('href').replace('/h/', '/l/')
	logger.info('URL home: ' + metadati.urlHome)

	// Tipo di origine?
	switch (this.richiesta.parametri.origine.tipo) {

	case 'riferimento':
		// 1. Singolo riferimento
		metadati.elementi = [ {
			tipo: 'scrittura',
			titolo: this.richiesta.parametri.origine.valore,
			riferimento: this.richiesta.parametri.origine.valore
		} ]
		break

	case 'url':
		// 2. Articolo
		metadati.elementi = await this.ottieniMetadatiSingoloArticolo({
			url: this.richiesta.parametri.origine.valore,
			prefisso: '',
			numerazione: 'paragrafo'
		})
		break

	case 'adunanza':
		// 3. Adunanza

		// Recupera l'href del pulsante 'Scrittura del giorno' e toglie la data (odierna)
		var urlBaseScritturaDelGiorno = $home('.todayNav').attr('href')

		// Calcola la data dall'offset
		logger.info('Offset: ' + this.richiesta.parametri.origine.offset + ' settimana/e')
		var data = require('moment')().add(this.richiesta.parametri.origine.offset, 'weeks')

		// Va alla pagina della scrittura del giorno nella data di cui abbiamo bisogno
		const risposta = await utils.richiestaHtml(costanti.urlBaseWol + urlBaseScritturaDelGiorno  + '/' + data.format('YYYY/MM/DD'))
		if (!risposta)
			throw 'Impossibile accedere alla Biblioteca Online'

		const $scritturaDelGiorno = risposta.jQuery
		logger.info('URL scrittura del giorno: ' + risposta.oggetto.request.uri.href)

		// In base al 'tipo' determina dove cercare il link all'articolo
		var url
		logger.info('Recupero l\'URL per l\'adunanza di tipo \'' + this.richiesta.parametri.origine.valore + '\' dalla pagina della scrittura del giorno...')

		switch (this.richiesta.parametri.origine.valore) {

		case 'w':
			// Torre di Guardia
			// Cerca la classe pub-w[anno], escludendo pub-ws[anno] (ediz. sempl.)
			url = [ $scritturaDelGiorno('[class*="pub-w"]:not([class*="pub-ws"]) .itemData a').attr('href') ]
			break

		case 'sbc':
		case 'cbs':
			// Studio biblico di congregazione
			// L'SBC è il terzultimo elemento di "Vita cristiana"
			var n =$scritturaDelGiorno('.christianLiving').next().find('li').length - 3
			url = utils.arrayHref($scritturaDelGiorno('.christianLiving').next().find('li').eq(n).find('a').not('.b'))
			break

		case 'vcm':
		case 'mwb':
			// Vita cristiana e ministero
			url = [ $scritturaDelGiorno('.itemCaption').eq(1).find('a').attr('href') ]
			break

		default:
			throw 'Tipo di adunanza non corretto o non gestito'

		}

		logger.info('URL per l\'adunanza di tipo \'' + this.richiesta.parametri.origine.valore + '\': ' + url)

		// Elabora l'URL trovato
		metadati.elementi = await this.ottieniMetadatiArticoliMultipli({
			url: url,
			prefisso: '',
			numerazione: 'paragrafo'
		})
		break

	}

	return metadati

}

//
// ## Elabora un *singolo* articolo e restituisce un'array con i metadati necessari a creare/scaricare i contenuti
// ## Se l'articolo è una "settimana" di VCM elabora ricorsivamente gli articoli in esso contenuti
// ## opzioni = { url (stringa), numerazione, prefisso }
//
WolParser.prototype.ottieniMetadatiSingoloArticolo = async function(opzioni) {

	logger.info('Elaboro singolo articolo ' + opzioni.url + ', cercando ' + JSON.stringify(this.richiesta.parametri.cerca))

	// Ciò che viene restituito alla fine della funzione
	var metadati = []

	// Se il link non inizia con "http" e contiene "/wol/" aggiunge l'URL base di WOL
	if (opzioni.url.indexOf('http') == -1 && opzioni.url.indexOf('/wol/') != -1)
		opzioni.url = costanti.urlBaseWol + opzioni.url

	// Per capire se è davvero un articolo e quindi va elaborato comunque esegue la richiesta, così da avere l'URL reale
	const risposta = await utils.richiestaHtml(opzioni.url)

	// Se non riesce ad accedere alla pagina non restituisce alcun contenuto
	if (!risposta)
		return []

	// Se il nome host è diverso da wol.jw.org salta questo "articolo", altrimenti prosegue
	if (risposta.oggetto.request.uri.hostname != 'wol.jw.org')
		return []

	// Se il prefisso è specificato gli aggiunge un punto, per migliorare la leggibilità
	if (opzioni.prefisso != '')
		opzioni.prefisso = opzioni.prefisso + '.'

	// Usa solo l'#article
	const $articolo = cheerio.load(risposta.jQuery('#article').html())

	// Da qui in poi la funzione è abbastanza "vecchio stile" e contiene molte sotto-funzioni, quindi rende this.richiesta direttamente accessibile
	const richiesta = this.richiesta

	// ### ALFA. Estrae metadati per link e video
	// Per ora si limita a recuperare il link, in seguito verrà controllato se è un semplice link o un video
	if (this.richiesta.parametri.cerca.link || this.richiesta.parametri.cerca.video)
		$($articolo('article a[href*=\'http\'], article a[href*=\'datalink\']')).each(function(i, e){

			// Per ogni link esterno a wol presente nell'articolo:

			var url = $(e).attr('href')
			// Aggiunge host, se necessario
			if (url.indexOf('/') == 0)
				url = costanti.urlBaseWol + url
			logger.info('Trovato URL esterno a wol: ' + url)

			// Lo aggiunge all'array dei link da elaborare
			metadati.push({
				tipo: 'link',
				titolo: $(e).text(),
				url: url
			})

		})

	// ### BETA. Estrae metadati per immagini e riferimenti
	if ($articolo('.treasures').length){

		// ## BETA 1. Adunanza VCM completa
		var indiceSBC

		// A. Articolo discorso iniziale
		metadati = metadati.concat(await this.ottieniMetadatiSingoloArticolo({
			// Url unico
			url: $articolo('.treasures').next().find('a').first().attr('href'),
			prefisso: '1T',
			numerazione: 'progressivo'
		}))

		// B. "Scaviamo" (solo riferimenti, presi direttamente da questa pagina)
		if (this.richiesta.parametri.cerca.riferimenti) {
			var htmlScritture = $articolo('.treasures').next().children('ul').children('li').eq(1).find('a.b').toArray()
			$(htmlScritture).each(function(i, e){
				// Determina il riferimento
				var riferimento = $(e).text().trim()
				if (riferimento != '')
					metadati.push({
						tipo: 'scrittura',
						paragrafo: '2T.' + (i + 1),
						titolo: riferimento,
						riferimento: riferimento
					})

			})
		}

		var $liVitaCristiana = $articolo('.christianLiving').next().find('ul:not(.noMarker) > li')

		// C. Prima parte di Vita cristiana
		// Per punti C e D: Prende il primo elemento a all'interno del li. Può essere un articolo interno di mwb o qualcos'altro.
		metadati = metadati.concat(await this.ottieniMetadatiArticoliMultipli({
			// Più di un url, potenzialmente
			url: utils.arrayHref($liVitaCristiana.eq(1).find('a').not('.b')),
			prefisso: '1VC',
			numerazione: 'progressivo'
		}))


		// D. Seconda parte di Vita cristiana (se presente)
		// La posizione dell'SBC varia, in base alla presenza di questa parte
		// Quindi visto che deve determinare se la parte esiste determina anche l'indiceSBC
		if ($liVitaCristiana.length > 5){
			logger.info('Seconda parta di \'Vita cristiana\': presente')
			indiceSBC = 3
			metadati = metadati.concat(await this.ottieniMetadatiArticoliMultipli({
				// Più di un url, potenzialmente
				url: utils.arrayHref($liVitaCristiana.eq(2).find('a').not('.b')),
				prefisso: '2VC',
				numerazione: 'progressivo'
			}))
		} else {
			logger.info('Seconda parta di \'Vita cristiana\': non presente')
			indiceSBC = 2
		}

		// E. Studio biblico di congregazione
		metadati = metadati.concat(await this.ottieniMetadatiArticoliMultipli({
			// Più di un url, potenzialmente
			url: utils.arrayHref($liVitaCristiana.eq(indiceSBC).find('a').not('.b')),
			prefisso: '3SBC'
		}))

	} else {

		// ### BETA 2. Articolo "semplice"

		// 0. Recupera l'url *reale* della pagina (cioè successivo ad eventuali redirect), da cui verranno estratti i paragrafi "bordeaux"
		var urlFinale = risposta.oggetto.request.uri.href
		logger.info('[url finale: ' + urlFinale + ']')

		var intervalloBlocchi = {}
		if (urlFinale != null)
			if (urlFinale.indexOf('#h=') != -1) {
				var estremi = urlFinale.substr(urlFinale.indexOf('#h=')+3).split('-')
				intervalloBlocchi.inizio = estremi[0].split(':')[0]
				intervalloBlocchi.fine = estremi[1].split(':')[0]
				// Se non esiste un elemento #p(fine) diminuisce il suo valore finché non ne trova uno esistente
				// Non serve?
				/*while ($(htmlPagina).find("#p" + intervalloBlocchi.fine).length == 0) {
					intervalloBlocchi.fine--
				}*/
				logger.info('Elaboro solo elementi id="p[' + intervalloBlocchi.inizio + '-' + intervalloBlocchi.fine + ']"')

				// Rimuove tutto quello che è fuori dall'intervallo
				var elementoFine = $articolo('#p' + intervalloBlocchi.fine)
				elementoFine.nextAll().remove()
				for (var i = 1; i < 10; i++) {
					const padreFine = $(elementoFine).parent()
					if (padreFine.hasClass('mock'))
						break
					else
						elementoFine.nextAll().remove()
					elementoFine = padreFine
				}

				// Rimuove tutto ciò che precede l'intervallo solo se intervalloBlocchi.inizio > 1
				if (intervalloBlocchi.inizio > 1) {
					var elementoInizio = $articolo('#p' + intervalloBlocchi.inizio)
					elementoInizio.prevAll().remove()
					for (var i = 1; i < 10; i++) {
						const padreInizio = $articolo(elementoInizio).parent()
						if (padreInizio.hasClass('mock'))
							break
						else
							elementoInizio.prevAll().remove()
						elementoInizio = padreInizio
					}
				}

			}

		// Determina se l'articolo ha i paragrafi numerati oppure no
		// (Confronta il numero di paragrafi (".sb") con il numero di numeri di pragrafic ("strong sup")
		const paragrafiNumerati = ($articolo('.sb').length - $articolo('strong sup').length < $articolo('.sb').length / 2)
		logger.info('Paragrafi numerati: ' + paragrafiNumerati)

		// 1A. Cicla su tutte le illustrazioni ("figure img") estraendo il paragrafo dalla didascalia se presente ("figcaption")
		if (this.richiesta.parametri.cerca.illustrazioni){
			var htmlIllustrazioni = $articolo('figure')
			$(htmlIllustrazioni).each(function(i, e){

				const $e = $(e)

				// Se il figure contiene un noscript ne "estrae" il contenuto
				$e.append($e.find('noscript').html())

				var numero
				if (opzioni.numerazione == 'paragrafo'){

					var paragrafo

					var tipoProgressivo = (paragrafiNumerati ? 'Par. ' : '') // Questo è il valore di default ma può diventare "Riquadro #"

					if ($e.parent().is(':first-child') && $e.parent().parent().is('article') && costanti.stringheImmagineIniziale[richiesta.parametri.lingua]) {

						// 1. L'illustrazione è all'inizio del articolo e stringheImmagineIniziale è disponibile nella lingua corrente: cerca di determinare il paragrafo cercando la domanda in cui si menziona l'illustrazione iniziale
						for (str of costanti.stringheImmagineIniziale[richiesta.parametri.lingua]) {
							paragrafo = parseInt($articolo('.qu:contains(\'' + str + '\')').text()) || paragrafo
						}

					} else if ($e.parents('.boxSupplement').length > 0 && costanti.stringheImmagineRiquadro[richiesta.parametri.lingua]) {

						// 2A. L'illustrazione è in riquadro e stringheImmagineRiquadro è disponibile nella lingua corrente: cerca di determinare il paragrafo cercando la domanda in cui si parla di riquadri
						for (str of costanti.stringheImmagineRiquadro[richiesta.parametri.lingua]) {
							paragrafo = parseInt($articolo('.qu:contains(\'' + str + '\')').text()) || paragrafo
						}

						// 2B. Se non trova la domanda contrassegna l'immagine come "in riquadro"
						if (!paragrafo) {
							tipoProgressivo = 'Riquadro #'
							var riquadro = $e.closest('.boxSupplement')[0]
							paragrafo = $articolo('.boxSupplement').toArray().indexOf(riquadro) + 1 // Se non trova niente restituisce -1 che diventa di nuovo 0
						}

					} else {

						// 3. Cerca di determinare il paragrafo dalla diascalia
						var didascalia = $e.find('figcaption p').html() || null
						logger.info(didascalia)
						if (didascalia) {

							// Prende il paragrafo dalla didascalia
							// Il numero del paragrafo è verso la fine della stringa ed è sempre preceduto da un'entita HTML non-breaking space (&nbsp; oppure &#160; oppure &#xa0;)

							var pos = didascalia.toLowerCase().lastIndexOf('&nbsp;')
							if (pos == -1)
								pos = didascalia.toLowerCase().lastIndexOf('&#160;')
							if (pos == -1)
								pos = didascalia.toLowerCase().lastIndexOf('&#xa0;')

							var numeroInDidascalia = parseInt(didascalia.substring(pos + 6))

							// Usa il numero trovato solo se non è troppo alto per essere un paragrafo
							if (numeroInDidascalia < 50)
								paragrafo = numeroInDidascalia

						}
					}

					if (!paragrafo || isNaN(paragrafo))
						// 4. Altrimenti determina il paragrafo dal <p> precedente
						paragrafo = parseInt($e.parent().prev().find('strong').text())

					if (!paragrafo || isNaN(paragrafo))
						// 5. Non c'è stato modo di determinare a che punto dell'articolo c'è l'immagine
						// Usa semplice numeratzione progressiva
						paragrafo = i + 1

					numero = opzioni.prefisso + tipoProgressivo + (paragrafo < 10 ? '0' + paragrafo : paragrafo)

				} else {

					// Richiesta semplice numerazione progressiva
					numero = opzioni.prefisso + (i + 1)

				}

				const titolo = $e.find('img').attr('alt') || ''

				// Se trova un attributo data-zoom (stessa immagine a risoluzione maggiore) all'interno del figure usa quello come src, altrimenti usa l'attributo src dell'img
				const dataZoom = $e.find('[data-zoom]').attr('data-zoom')
				var src = ''
				if (dataZoom)
					src = dataZoom
				else
					src = $e.find('img').attr('src')


				// Aggiunge "http://wol.jw.org" all'src, se necessario
				if (src.indexOf('http') != 0)
					src = costanti.urlBaseWol + src

				metadati.push({
					tipo: 'illustrazione',
					locale: false,
					paragrafo: numero,
					ordine: numero, // ordine = paragrafo
					titolo: titolo,
					src: src
				})

			})
		}

		// 1B. Cicla su tutte le "modalità di visualizzazione alternative"
		if (this.richiesta.parametri.cerca.illustrazioni){
			var htmlVisualizzazioni = $articolo('.alternatePresentation')
			$(htmlVisualizzazioni).each(function(i, e){

				var numero = opzioni.prefisso + '0'
				metadati.push({
					tipo: 'illustrazione',
					locale: false,
					paragrafo: numero,
					ordine: numero, // ordine = paragrafo
					titolo: 'Visualizzazione alternativa',
					src: $(e).find('img').attr('src')
				})

			})
		}

		// 2. Cicla su tutti i link a versetti che corrispondono a "a.b strong"
		if (this.richiesta.parametri.cerca.riferimenti){
			var riferimentoPrecedente
			var htmlScritture = $articolo('a.b strong').parent().toArray()
			$(htmlScritture).each(function(i, e){

				// Determina il paragrafo
				var paragrafo = parseInt($(e).closest('.sb').text())
				// Se manca l'elemento .sb allora è il primo paragrafo
				if (isNaN(paragrafo))
					paragrafo = 1

				// Determina il riferimento
				var riferimento = $(e).find('strong').text().trim()
				if (riferimento == ''){
					libero = true
					return
				}

				if (typeof riferimentoPrecedente !== 'undefined'){
					if (riferimento.indexOf(':') == -1)
						// Il riferimento è solo un versetto (mancano i ":"), senza libro e capitolo: li prende dal rif. precedente
						// Es.: ["Romani 3:2", "5"] -> "Romani 3:5"
						riferimento = riferimentoPrecedente.substr(0, riferimentoPrecedente.indexOf(':') + 1) + riferimento
					else if (riferimento.match(/[1234567890 :;,.-]+$/) == riferimento){
						// Il riferimento è solo capitolo:versetto (contiene i ":" ma non c'衮essun nome di libro): prende il nome del libro dal rif. precedente
						// Es.: ["Romani "1:10", "4:12"] -> "Romani 4:12"
						libroRifPrecedente = riferimentoPrecedente.substr(0, riferimentoPrecedente.indexOf(':'))
						libroRifPrecedente = libroRifPrecedente.substr(0, libroRifPrecedente.lastIndexOf(' '))
						riferimento = libroRifPrecedente + ' ' + riferimento
					}
				}

				// Salva il riferimento attuale per la prossima ricerca
				riferimentoPrecedente = riferimento

				// Rimuove ";" e "," al termine del riferimento
				if (riferimento.substr(riferimento.length - 1) == ',')
					riferimento = riferimento.substr(0, riferimento.length - 1)
				if (riferimento.substr(riferimento.length - 1) == ';')
					riferimento = riferimento.substr(0, riferimento.length - 1)

				metadati.push({
					tipo: 'scrittura',
					paragrafo: opzioni.prefisso + 'Par. ' + (paragrafo < 10 ? '0' + paragrafo : paragrafo),
					titolo: riferimento,
					riferimento: riferimento
				})

			})

		}

		logger.info('Elaborazione singolo articolo ' + opzioni.url + ' terminata, trovati ' + metadati.length + ' elementi')

	}
	return metadati

}

//
// ## Wrapper di ottieniMetadatiSingoloArticolo, che lancia l'elaborazione di più articoli contemporaneamente
// ## e restituisce tutti i metadati insieme
// ## opzioni = { url (array di stringhe), numerazione, prefisso }
//

WolParser.prototype.ottieniMetadatiArticoliMultipli = async function(opzioni){

	// Tutto finirà qui dentro
	var metadati = []

	logger.info('Ottengo i metadati per gli articoli:')
	logger.info(opzioni.url)

	for (var url of opzioni.url) {

		metadati = metadati.concat(await this.ottieniMetadatiSingoloArticolo({
			url: url,
			numerazione: opzioni.numerazione,
			prefisso: opzioni.prefisso
		}))

	}

	return metadati

}

module.exports = WolParser
