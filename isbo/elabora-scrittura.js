// Questo modulo si occupa di creare e restituire i contenuti di tipo "scrittura"

const Elaboratore = require('./elabora-contenuti.js')

//
// ## Elabora una scrittura
// ## Formato dell'oggetto "scrittura":
// ## { tipo: 'scrittura', titolo: (stringa), riferimento: (stringa) }
// ## Restituisce l'oggetto scrittura con in più la proprietà "canvas"
//
Elaboratore.prototype.elaboraScrittura = async function(scrittura) {

	logger.info('Elaboro scrittura: ' + scrittura.riferimento + ' (titolo: ' + scrittura.titolo + ')')

	// Chiede a WOL il testo del riferimento
	const rispostaScrittua = await utils.richiestaHtml(this.richiesta.metadati.urlHome + '?q=' + scrittura.riferimento)

	// Se non è stato possibile ottenere la pagina restituisce null
	if (!rispostaScrittua)
		return null

	var $versetti = rispostaScrittua.jQuery('li.bibleCitation .v')

	// Particolarità: classe "altsize"
	$versetti.find('.altsize').each(function(i, e) {
		$(e).replaceWith($(e).text())
	})

	// Prende solo il testo contenuto *direttamente* negli elementi "li.bibleCitation .v"
	// http://stackoverflow.com/questions/3442394/jquery-using-text-to-retrieve-only-text-not-nested-in-child-tags
	// append("&nbsp;") serve a garantire che ci sia uno spazio tra i versetti
	var testo = $versetti.append('&nbsp;').clone().children().remove().end().text()

	// Iniziale maiuscola, niente spazi all'inizio o alla fine
	testo = testo.capitalizeFirstLetter().trim()

	// Rimuove parentesi quadre
	testo = testo.replace(/\[/g, '').replace(/\]/g, '')

	// Se l'ultimo carattere è una virgola, due punti o un punto e virgola lo trasforma in un punto
	var ultimoCarattere = testo.substr(testo.length - 1)
	if (ultimoCarattere == ':' || ultimoCarattere == ';' || ultimoCarattere == ',')
		testo = testo.substr(0, testo.length - 1) + '.'

	logger.info('Testo riferimento: ')

	// Rende disponibile globalmente la lunghezza dell'ultimo riferimento elaborato
	this.lunghezzaUltimoRiferimento = testo.length

	if (testo.length > parseInt(this.richiesta.parametri.lunghezzaMaxRiferimenti)) {
		// Testo nullo o troppo lungo: salta
		logger.info('Riferimento \'' + scrittura.riferimento + '\' ignorato (lunghezza: ' + testo.length + ')')
		// Se questo riferimento costituisce l'intera richiesta solleva un'eccezione
		if (this.richiesta.parametri.origine.tipo == 'riferimento')
			throw 'Testo del riferimento troppo lungo'
		// Altrimenti restituisce null
		return null
	} else if (testo.length == 0) {
		// Testo nullo o troppo lungo: salta
		logger.info('Riferimento \'' + scrittura.riferimento + '\' ignorato (non valido)')
		// Se questo riferimento costituisce l'intera richiesta solleva un'eccezione
		if (this.richiesta.parametri.origine.tipo == 'riferimento')
			throw 'Riferimento non valido'
		// Altrimenti restituisce null
		return null
	}

	// Crea il canvas
	logger.info('Creo il canvas per "' + scrittura.riferimento + '": "' + testo + '"')

	// Crea il canvas e ne estrai il context
	var canvas = new Canvas(this.dimensioniCanvas.larghezza, this.dimensioniCanvas.altezza)
	var ctx = canvas.getContext('2d')

	// Sfondo nero
	ctx.fillStyle = '#000000'
	ctx.fillRect(0, 0, this.dimensioniCanvas.larghezza, this.dimensioniCanvas.altezza)

	// Immagine di sfondo (se prevista)
	if (this.richiesta.parametri.sfondo == 'img' && sfondo)
		ctx.drawImage(sfondo, 0, 0, this.dimensioniCanvas.larghezza, this.dimensioniCanvas.altezza)

	// Colore testi (bianco)
	ctx.fillStyle = '#FFFFFF'

	var dimFontRiferimento = 100 // Spazio occupato dal riferimento, in basso
	var margineLati = 0.07 // Margine sui lati (come frazione di larghezza e altezza)

	// Spazio a disposizione per testo scrittura
	var altezzaDisponibilie = this.dimensioniCanvas.altezza * (1 - margineLati * 2) - dimFontRiferimento
	var larghezzaDisponibile = this.dimensioniCanvas.larghezza * (1 - margineLati * 2)

	// Contiene caratteri CJK?
	const font = (/[\u3400-\u9FBF]/.test(testo) ? '\'Microsoft YaHei\'' : 'Merriweather')

	// Stampa il testo
	drawMultilineText(
		ctx,
		testo,
		{
			rect: {
				x: canvas.width * margineLati,
				y: canvas.height * margineLati,
				width: larghezzaDisponibile,
				height: altezzaDisponibilie
			},
			font: font,
			verbose: true,
			logFunction: function(messaggio) { logger.info(messaggio) }
		}
	)

	// Stampa il riferimento
	var riferimentoDaStampare = scrittura.riferimento.trim()
	if (riferimentoDaStampare > 20)
	// Se il riferimento è più lungo di 20 caratteri usa una dimensione inferiore
		dimFontRiferimento *= 0.75
	ctx.font = dimFontRiferimento + 'px ' + font
	ctx.fillText(
		riferimentoDaStampare,
		this.dimensioniCanvas.larghezza * (1 - margineLati) - ctx.measureText(riferimentoDaStampare).width,
		this.dimensioniCanvas.altezza * (1 - margineLati)
	)

	logger.info('Generato canvas:')
	logger.info(canvas)

	// Aggiunge il canvas all'elemento e lo restituisce
	scrittura.canvas = canvas
	return scrittura

}
