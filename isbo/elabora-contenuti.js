// Questo modulo si occupa di smistare la creazione e restituire i contenuti veri e propri (imagini, video, url),
// mettendo a disposzione la classe Elaboratore.
// Prevede che la richiesta ricevuta come argomento contenga gli oggetti "parametri" e "metadati"
// Per mantenere il repo ordinato i metodi della classe Elaboratore sono distribuiti sui file elabora-*.js
// Utilizzo:
//	const Elaboratore = require('./elabora-contenuti.js')
//	const contenuti = await new Elaboratore(richiestaExpress).elabora()
//
// ## Elaboratore della classe WolParser
//
var Elaboratore = function(richiesta) {

	// Oggetto richiesta di express disponibile ovunque nell'oggetto WolParser
	// Serve per accedere a richiesta.parametri e richiesta.metadati
	this.richiesta = richiesta

	// Titoli degli elementi già elaborati - Serve a non restituire due volte lo stesso contenuto
	this.elementiElaborati = []

	// Sotto-moduli, contenenti le funzioni di Elaboratore relative ai tipi di contenuto
	require('./elabora-scrittura.js')
	require('./elabora-illustrazione.js')
	require('./elabora-link.js')

}

//
// ## Avvia le funzioni di elaborazione di tutti i metadati
//
Elaboratore.prototype.elabora = async function() {

	logger.info('Elaboro i metadati:')
	logger.info(this.richiesta.metadati)
	logger.info('Parametri richiesta:')
	logger.info(this.richiesta.parametri)

	// Prepara alcuni dati che serviranno per l'elaborazione

	// Dimensiona i canvas in base al rapporto
	this.dimensioniCanvas = { larghezza: 1280 }
	this.dimensioniCanvas.altezza = Math.round(parseFloat(this.dimensioniCanvas.larghezza / this.richiesta.parametri.rapportoImmagini))
	logger.info('Dimensione dei canvas: ' + this.dimensioniCanvas.larghezza + 'x' + this.dimensioniCanvas.altezza)

	// Tutto finirà qui dentro
	var contenuti = []

	// Avvia in parallelo tutte le funzioni di elaborazione
	let elaborazioni = this.richiesta.metadati.elementi.map(elemento => {

		// Già elaborato?
		if (this.elementiElaborati.includes(elemento.titolo.toLowerCase())) {

			console.log('Elemento \'' + elemento.titolo + '\' ignorato (già elaborato)')
			return null

		} else {

			this.elementiElaborati.push(elemento.titolo.toLowerCase())

			// Decide cosa fare in base al tipo
			switch(elemento.tipo){

			case 'scrittura':
				return this.elaboraScrittura(elemento)

			case 'illustrazione':
				return this.elaboraIllustrazione(elemento)

			case 'link':
				return this.elaboraLink(elemento)

			}

		}

	})

	for (let elaborazione of elaborazioni) {
		const contenuto = await elaborazione
		// Se l'esito dell'elaborazione è null non lo aggiunge all'array
		if (contenuto)
			contenuti = contenuti.concat(contenuto)
	}

	// Quando sono tutti elaborati restituisce l'array dei contenuti
	return contenuti

}

module.exports = Elaboratore
