// Questo modulo si occupa di creare e restituire i contenuti di tipo "illustrazione"

const Elaboratore = require('./elabora-contenuti.js')

//
// ## Elabora un'illustrazione
// ## Formato dell'oggetto "illustrazione":
// ## { tipo: 'illustrazione', titolo: (stringa), locale: (bool), src: (stringa) }
// ## Restituisce l'oggetto illustrazione con in più la proprietà "buffer"
//
Elaboratore.prototype.elaboraIllustrazione = async function(illustrazione) {

	logger.info('Elaboro illustrazione: ' + illustrazione.src)

	try {

		// Scarica su disco e converte in PNG (JPEG non è supportato da node-canvas 1.x)
		const immagine = await utils.getPngDaUrl(illustrazione.src)

		// La logica è la stessa per immagini locali e remote

		// Calcola alcuni dati relativi all'immagine necessari a sapere come gestirla
		// 1. Calcola rapporto immagine
		immagine.rapporto = immagine.width / immagine.height
		// Arrotonda sia il rapporto dell'immagine che del canvas a 4 decimali
		immagine.rapporto = Math.round(immagine.rapporto * 10000) / 10000
		this.richiesta.parametri.rapportoImmagini = Math.round(this.richiesta.parametri.rapportoImmagini * 10000) / 10000
		logger.info('Rapporto immagine: ' + immagine.rapporto)

		// 2. Calcola dimensioni immagine rispetto al canvas
		// 3. Determina su quali bordi avanza spazio
		if (immagine.rapporto == this.richiesta.parametri.rapportoImmagini) {
			// L'immagine ha lo stesso identico rapporto del canvas
			logger.info('L\'immagine ha lo stesso identico rapporto del canvas')
			immagine.larghezzaPerCanvas = this.dimensioniCanvas.larghezza
			immagine.altezzaPerCanvas = this.dimensioniCanvas.altezza
			immagine.avanzaSpazioLati = false
			immagine.avanzaSpazioSopraSotto = false
		} else if (immagine.rapporto > this.richiesta.parametri.rapportoImmagini) {
			// L'immagine è più "larga" del canvas: ci saranno dei margini sopra e sotto
			logger.info('L\'immagine è più larga del canvas: ci saranno dei margini sopra e sotto')
			immagine.larghezzaPerCanvas = this.dimensioniCanvas.larghezza
			immagine.altezzaPerCanvas = parseInt(immagine.larghezzaPerCanvas / immagine.rapporto)
			immagine.avanzaSpazioLati = false
			immagine.avanzaSpazioSopraSotto = true
		} else if (immagine.rapporto < this.richiesta.parametri.rapportoImmagini) {
			// L'immagine è più "stretta" del canvas: ci saranno dei margini ai lati
			logger.info('L\'immagine è più stretta del canvas: ci saranno dei margini ai lati')
			immagine.altezzaPerCanvas = this.dimensioniCanvas.altezza
			immagine.larghezzaPerCanvas = parseInt(immagine.altezzaPerCanvas * immagine.rapporto)
			immagine.avanzaSpazioLati = true
			immagine.avanzaSpazioSopraSotto = false
		}
		logger.info('Dimensioni originali immagine: ' + immagine.width + 'x' + immagine.height)
		logger.info('Dimensioni immagine su canvas: ' + immagine.larghezzaPerCanvas + 'x' + immagine.altezzaPerCanvas)

		// Crea il canvas e ne estrai il context
		var canvas = new Canvas(this.dimensioniCanvas.larghezza, this.dimensioniCanvas.altezza)
		var ctx = canvas.getContext('2d')

		// Se avanza dello spazio ai margini cerca il modo migliore di riempirlo
		if (immagine.avanzaSpazioLati || immagine.avanzaSpazioSopraSotto) {

			// Cerca colori modali sui margini
			var coloriModali

			const porzioneMargini = 0.015 // 1.5%

			if (immagine.avanzaSpazioLati)
				coloriModali = [
					immagine.estraiCanvasMargine('sinistra', porzioneMargini).coloreModale(),
					immagine.estraiCanvasMargine('destra', porzioneMargini).coloreModale()
				]
			else
				coloriModali = [
					immagine.estraiCanvasMargine('sopra', porzioneMargini).coloreModale(),
					immagine.estraiCanvasMargine('sotto', porzioneMargini).coloreModale()
				]

			if (coloriModali[0] && coloriModali[1]) {

				// A. Colori unici ai margini
				logger.info('Applico colori modali: ' + coloriModali[0] + ' e ' + coloriModali[1])

				if (immagine.avanzaSpazioLati) {
					ctx.fillStyle = coloriModali[0]
					ctx.fillRect(0, 0, this.dimensioniCanvas.larghezza / 2, this.dimensioniCanvas.altezza)
					ctx.fillStyle = coloriModali[1]
					ctx.fillRect(this.dimensioniCanvas.larghezza / 2, 0, this.dimensioniCanvas.larghezza / 2, this.dimensioniCanvas.altezza)
				} else {
					ctx.fillStyle = coloriModali[0]
					ctx.fillRect(0, 0, this.dimensioniCanvas.larghezza, this.dimensioniCanvas.altezza / 2)
					ctx.fillStyle = coloriModali[1]
					ctx.fillRect(0, this.dimensioniCanvas.altezza / 2, this.dimensioniCanvas.larghezza, this.dimensioniCanvas.altezza / 2)
				}

			} else {

				// B. Blur
				logger.info('Applico blur')

				// Estrae degli "spicchi" a sinistra e a destra e li ridimensiona per occupare tutto lo spazio vuoto ai lati
				var porzioneSpicchi = 0.2

				// La dimensione degli spicchi non sarà mai maggiore di quella dell'area vuota nel canvas che dovranno coprire
				var spazioVuotoLatiImmagine = (immagine.avanzaSpazioSopraSotto ? (this.dimensioniCanvas.altezza - immagine.altezzaPerCanvas) : (this.dimensioniCanvas.larghezza - immagine.larghezzaPerCanvas)) / 2
				var porzioneVuotaLatiImmagine = spazioVuotoLatiImmagine / (immagine.avanzaSpazioSopraSotto ? this.dimensioniCanvas.altezza : this.dimensioniCanvas.larghezza )
				if (porzioneSpicchi > porzioneVuotaLatiImmagine)
					porzioneSpicchi = porzioneVuotaLatiImmagine

				// Gli spicchi vengono ingranditi di n pixel per lato, per evitare strisce bianche che poi incasinano il blur
				const pxCrop = 3
				if (immagine.avanzaSpazioLati) {

					// Estrae il primo spicchio e lo espande su tutto il margine sinistro
					ctx.drawImage(
						immagine.estraiCanvasMargine('sinistra', porzioneSpicchi),
						-pxCrop,
						-pxCrop,
						spazioVuotoLatiImmagine + (pxCrop * 2),
						this.dimensioniCanvas.altezza + (pxCrop * 2)
					)

					// Estrae l'ultimo spicchio e lo espande su tutto il margine destro
					ctx.drawImage(
						immagine.estraiCanvasMargine('destra', porzioneSpicchi),
						this.dimensioniCanvas.larghezza - spazioVuotoLatiImmagine - pxCrop,
						-pxCrop,
						spazioVuotoLatiImmagine + (pxCrop * 2),
						this.dimensioniCanvas.altezza + (pxCrop * 2)
					)

				} else if (immagine.avanzaSpazioSopraSotto) {

					// Estrae il primo spicchio e lo espande su tutto il margine superiore
					ctx.drawImage(
						immagine.estraiCanvasMargine('sopra', porzioneSpicchi),
						-pxCrop,
						-pxCrop,
						this.dimensioniCanvas.larghezza + (pxCrop * 2),
						spazioVuotoLatiImmagine + (pxCrop * 2)
					)

					// Estrae l'ultimo spicchio e lo espande su tutto il margine inferiore
					ctx.drawImage(
						immagine.estraiCanvasMargine('sotto', porzioneSpicchi),
						-pxCrop,
						this.dimensioniCanvas.altezza - spazioVuotoLatiImmagine - pxCrop,
						this.dimensioniCanvas.larghezza + (pxCrop * 2),
						spazioVuotoLatiImmagine + (pxCrop * 2)
					)

				}

				// Prima di applicare il blur inserisce comunque l'immagine al centro del canvas, con la massima grandezza possibile
				ctx.drawImage(
					immagine,
					(this.dimensioniCanvas.larghezza - immagine.larghezzaPerCanvas) / 2,
					(this.dimensioniCanvas.altezza - immagine.altezzaPerCanvas) / 2,
					immagine.larghezzaPerCanvas,
					immagine.altezzaPerCanvas
				)

				// Blur
				StackBlur.canvasRGBA(canvas, 0, 0, this.dimensioniCanvas.larghezza, this.dimensioniCanvas.altezza, 180)

			}

		}

		// Inserisce l'immagine al centro del canvas, con la massima grandezza possibile
		ctx.drawImage(
			immagine,
			(this.dimensioniCanvas.larghezza - immagine.larghezzaPerCanvas) / 2,
			(this.dimensioniCanvas.altezza - immagine.altezzaPerCanvas) / 2,
			immagine.larghezzaPerCanvas,
			immagine.altezzaPerCanvas
		)

		// Converte l'oggetto sharp in buffer e lo aggiunge all'elemento
		//illustrazione.buffer = await sharpImmagineFinale.toBuffer()
		illustrazione.canvas = canvas

		logger.info('Generato canvas:')
		logger.info(canvas)

		// Se l'immagine è stata uplodata cancella l'src originale
		if (illustrazione.locale)
			fs.unlink(illustrazione.src, function() { /* Evita DeprecationWarning */ })

		// Restituisce subito l'illustrazione o cerca sotto-immagini
		var output = illustrazione

		// Sotto-immagini?
		if (this.richiesta.parametri.dividiImmaginiComposte) {
			const DivisoreImmagini = require('./divisore-immagini.js')
			const sottoImmagini = await (new DivisoreImmagini(illustrazione, immagine, this)).dividi()
			output = sottoImmagini.concat(output)
		}

		return output

	} catch (e) {
		// Errore più frequente: alcune immagini sono JPEG e non PNG - Le salta
		logger.info('Impossibile accede all\'illustrazione ' + illustrazione.titolo)
		return null
	}

}
