// Questo modulo si occupa crerare un'archivio in locale con dentro tutti i contenuti

//
// ## Crea un archivio zip sul filesystem locale
// ## Una volta terminato restituisce il percorso locale dell'archivio
// ## L'argomento "richiesta" deve contenere gli oggetti "parametri", "metadati" e "contenuti"
//
exports.creaArchivio = async function(richiesta) {

	logger.info('Creo un archivio con i ' + richiesta.contenuti.length + ' contenuti estratti')

	var linkAggunti = []

	// Inizializza jszip
	const JSZip = require('jszip')
	var zip = new JSZip()

	// Sviluppo: aggiunge all'archivio un file fisso, per testare il frontend
	// zip.file("asd.mp4", fs.readFileSync(path.join(cartellaRoot, 'var/jwbrd_I_201511_02_r240P.mp4')))

	// Aggiunge i contenuti
	for (contenuto of richiesta.contenuti) {

		// Salta eventuali contenuti "null"
		if (contenuto) {

			// Per ogni contenuto valido decide come aggiungerlo all'archivio in base al tipo
			if (contenuto.tipo == 'scrittura' || contenuto.tipo == 'illustrazione') {

				// Aggiunge il canvas
				logger.info('Aggiungo all\'archivio l\'immagine: ' + contenuto.titolo)

				var nomeFile = contenuto.titolo.substr(0, 99).sanitizzaStringaPerNomeFile() + '.png'
				if (contenuto.paragrafo)
					nomeFile = contenuto.paragrafo + ' - ' + nomeFile

				zip.file(
					nomeFile,
					// Usa base64
					contenuto.canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''),
					{ base64: true }
				)

			} else {

				// Aggiunge il link o direttamnete il file

				// Già aggiunto?
				if (linkAggunti.indexOf(contenuto.url.toLowerCase()) != -1)
					logger.info('Link ignorato (già aggiunto all\'archivio)')
				else {

					linkAggunti.push(contenuto.url.toLowerCase())

					// Azioni diverse in base alla presenza di un link diretto a un file
					if (contenuto.urlDirettoFile && richiesta.parametri.cerca.video) {

						// È un file e il client ha richiesto i video: aggiunge direttamente il file
						logger.info('Scarico e aggiungo all\'archivio il file: ' + contenuto.urlDirettoFile)
						zip.file(
							contenuto.titolo.sanitizzaStringaPerNomeFile() + '.' + contenuto.urlDirettoFile.estensioneFile(),
							request.get(contenuto.urlDirettoFile)
						)

					} else {

						// Non è un file o il client non ha richiesto i video: aggiunge semplicemente l'URL
						logger.info('Creo e aggiungo all\'archivio il file .url: ' + contenuto.url)
						zip.file(contenuto.titolo.sanitizzaStringaPerNomeFile() + '.url', contenuto.fileUrl)

					}

				}

			}

		}

	}

	// Inizia a creare l'archivio
	logger.info('Inizio a creare l\'archivio...')
	var percorsoLocaleArchivio = path.join(cartellaRoot, 'tmp/output-' + rnd.generate(10) + '.zip')

	var stream = zip.generateNodeStream({
		type: 'nodebuffer',
		streamFiles: true
	}).pipe(fs.createWriteStream(percorsoLocaleArchivio))

	// Restituisce una promise
	return new Promise(function(resolve, reject) {
		stream.on('finish', () => {
			logger.info('Creazione archivio completata')
			resolve(percorsoLocaleArchivio)
		})
		stream.on('error', reject)
	})

}

//
// ## Aggiunge un file di tipo "link" a un archizio zip
//
aggiungiFileUrl = function(zip, contenuto) {
	zip.file(contenuto.titolo.sanitizzaStringaPerNomeFile() + '.url', '[InternetShortcut]\r\nURL=' + contenuto.url)
}
