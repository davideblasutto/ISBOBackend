// Middleware express personalizzato

module.exports = function(richiesta, risposta, prossimo) {

	// 1. Azioni eseguite per ogni richiesta

	// - Log su database
	connessioneMySQL.query(
		'INSERT INTO Richiesta SET ?',
		{
			IP: richiesta.ip,
			Metodo: richiesta.method,
			Endpoint: richiesta.path,
			Parametri: CircularJSON.stringify(richiesta.query)
		},
		function (errore, risultato) {
			if (errore) {
				logger.info('Errore non bloccante in una query MySQL:')
				logger.info(errore)
			} else {
				// Da qui in poi l'ID del record è memorizzato nella richiesta
				richiesta.id = risultato.insertId
			}
		}
	)


	// 2. Funzioni messe a disposizione dal middleware

	//
	// ## Restituisce un file (archivio) disponibile in locale
	//
	risposta.consegna = function(percorsoLocaleFile, nomeFile) {

		return risposta.download(percorsoLocaleFile, nomeFile, function(errore) {

			if (errore)
				throw errore
			else
				logger.info('File ' + percorsoLocaleFile + ' inviato al client con nome ' + nomeFile)

			// Comunque cancella il file locale
			fs.unlink(percorsoLocaleFile, function() { /* Evita DeprecationWarning */ })

			// Logga il termine dell'operazione
			connessioneMySQL.query(
				'UPDATE Richiesta SET TimestampFine = NOW(), ? WHERE ID = ' + richiesta.id,
				{
					Esito: true,
					Metadati: CircularJSON.stringify(richiesta.metadati)
				}
			)

		})

	}

	//
	// ## Restituisce un errore
	//
	risposta.errore = function(errore) {

		logger.info('Risposta terminata con errore: ')
		logger.info(errore)

		// Logga il termine dell'operazione
		connessioneMySQL.query(
			'UPDATE Richiesta SET TimestampFine = NOW(), ? WHERE ID = ' + richiesta.id,
			{
				Errore: errore
			}
		)

		risposta.set({ 'content-type': 'text/plain; charset=utf-8' })

		// Decide che informazione dare al client
		var messaggioErrore = ''
		if (typeof errore == 'string')
			// Si tratta al 99% di un throw "intenzionale" legato alla logica del backend
			messaggioErrore = errore
		else if (errore.name == 'RequestError')
			// Errore in una richiesta HTTP (verso jw.org)
			messaggioErrore = 'Impossibile raggiungere jw.org - Riprovare più tardi'
		else
			// Altro errore
			messaggioErrore = 'Errore interno'

		// Il codice di errore è variabile
		risposta.status((richiesta.get('Frontend-Web') == 'true') ? 299 : 400).end(messaggioErrore)

	}

	prossimo()

}
